﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5TestProj.ViewModels
{
    public class CropGroup
    {
        public int CropId { get; set; }
        public string Name { get; set; }
        public int VarietyCount { get; set; }

    }
}
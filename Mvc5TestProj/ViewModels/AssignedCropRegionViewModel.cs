﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5TestProj.ViewModels
{
    public class AssignedCropRegionViewModel
    {
        public int CropRegionId { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}
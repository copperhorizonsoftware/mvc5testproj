﻿using Mvc5TestProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5TestProj.ViewModels
{
    public class ExternalPartnerIndexViewModel
    {
        public IEnumerable<ExternalPartner> ExternalPartners { get; set; }

        public IEnumerable<CropRegion> CropRegions { get; set; }
    }
}
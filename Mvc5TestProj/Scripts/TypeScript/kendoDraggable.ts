﻿///<reference path='../../Scripts/typings/jquery/jquery.d.ts' />
///<reference path='../../Scripts/typings/kendo/kendo.all.d.ts' /> 
$(function () {
    $(function () {
        function draggableOnDragStart(e) {
            $(e.target).addClass("box_current");
            $('#lblCount').html("(Drop inside a box!)");
        }

        function droptargetOnDragEnter(e) {
            $('#lblCount').html("Now drop...");
        }

        function droptargetOnDragLeave(e) {
            $('#lblCount').html("(Drop inside a box!)");
        }

        function acceptBoxIngallery(draggable, container) {
             $(container).append($(draggable));
        }

        function draggableOnDragEnd(e) {
            var draggable = $("#test-container");

            if (!draggable.data("kendoDraggable").dropped) {
                // drag ended outside of any droptarget
                $('#lblCount').html("Try again!")
            }

            $(".box").removeClass("box_current");
        }

        galleryDragAndDrop("#test-container", "#gallery-one", "#gallery-two", ".box");

        function galleryDragAndDrop(mainContainer, containerOne, containerTwo, dragItem) {
            $(mainContainer).kendoDraggable({
                filter: '.box',
                hint: function (item) {
                    return $(item).clone();
                },
                dragstart: draggableOnDragStart,
                dragend: draggableOnDragEnd
            });

            $(containerOne).kendoDropTarget({
                dragenter: droptargetOnDragEnter,
                dragleave: droptargetOnDragLeave,
                drop: function (e) {
                    $('#lblCount').html("You did great!");
                     acceptBoxIngallery($(e.draggable.currentTarget), containerOne);
                },
            });

            $(containerTwo).kendoDropTarget({
                dragenter: droptargetOnDragEnter,
                dragleave: droptargetOnDragLeave,
                drop: function (e) {
                    $('#lblCount').html("You did great!");
                    acceptBoxIngallery($(e.draggable.currentTarget), containerTwo);
                },
            });
        }
    });
});  
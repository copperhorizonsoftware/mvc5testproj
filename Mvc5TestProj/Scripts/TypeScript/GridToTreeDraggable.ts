﻿///<reference path='../../Scripts/typings/jquery/jquery.d.ts' />
///<reference path='../../Scripts/typings/kendo/kendo.all.d.ts' /> 

var treeView;
var dataGrid;
var nodeSelectedOnDrop : boolean = false;
var selectedUserId : number = 0;
var responsibleUserForFruitId : number = 0;

$(function () {
    treeView = $("#treeView").data("kendoTreeView");

    $("#treeView").kendoDropTarget({
        drop: onDrop,
        dragenter: function (e) {
            (<any> e.draggable).hint.css("opacity", 0.6); //modify the draggable hint
        },
        dragleave: function (e) {
            (<any> e.draggable).hint.css("opacity", 1); //modify the draggable hint
        }
    });

    dataGrid = $("#policyGrid").data("kendoGrid");

    // drag and drop from the policyGrid
    dataGrid.table.kendoDraggable({
        filter: "tbody > tr",
        cursorOffset: {
            top: 10,
            left: 10
        },
        hint: function (e) {
            //return $('<div class="k-grid k-widget"><table><tbody><tr>' + e.html() + '</tr></tbody></table></div>');
            //var itemUID = e.attr(kendo.attr("uid"));
            responsibleUserForFruitId = dataGrid.dataItem(e).responsibleUserid;

            return $('<div class="k-grid k-widget">' + dataGrid.dataItem(e).name + '</div>');
        },
        dragstart: draggableOnDragStart
    });

    $("#treeView").on('mousedown', '.k-item', function (event) {

    });

    function onDrop(e) {
        var elem = e.target ? e.target : e.toElement;

        // Mark the node as selected
        treeView.select($(elem));
        nodeSelectedOnDrop = true;

        // Trigger the event on the selected node.
        treeView.trigger('select', {
            node: $(elem)
        });

        //alert(itemUID);
        //itemUID = null;
    }

    function draggableOnDragStart(e) {
        dataGrid.select($(e.currentTarget).closest("tr"));
    }
});

function onSelect(e) {
    if (nodeSelectedOnDrop == true) {
        alert("Tree node user id: " + e.sender.dataItem(e.node).userid);
        selectedUserId = e.sender.dataItem(e.node).userid;
        if (!treeView.dataItem(e.node).hasChildren) {
            if (responsibleUserForFruitId != selectedUserId) {
                var grid = $("#policyGrid").data("kendoGrid");
                var row = grid.select();
                var data = grid.dataItem(row);
                if (data != null) {
                    alert("Fruit selected in grid: " + data.name);
                    var dataRows = grid.items();
                    var rowIndex = dataRows.index(grid.select());
                    var theItem = grid.dataSource.data()[rowIndex];
                    theItem.set('responsibleUserid', selectedUserId);
                }
            }
        } else {
            alert("This is a group leader. Cannot be assigned a fruit.")
        }
    }

    nodeSelectedOnDrop = false;
}

function treeView_expand() {
    //Handle the expand event

}

function onDataBound() {
    treeView.expand(".k-item");
}




﻿using Mvc5TestProj.Models;
using Mvc5TestProj.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc5TestProj.Controllers
{
    public class HomeController : Controller
    {
        private Mvc5TestEntities db = new Mvc5TestEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            // LINQ version.
            IQueryable<CropGroup> data =
                from cv in db.CropVarieties
                join c in db.Crops
                on cv.CropId equals c.CropId
                group cv by c.Name into crGroup
                orderby crGroup.Key
                select new CropGroup
                {
                    Name = crGroup.Key,
                    VarietyCount = crGroup.Count()
                };

            // SQL version of the above LINQ code.
            //string query = "SELECT crop.Name, COUNT(*) AS VarietyCount "
            //    + "FROM CropVarieties AS cv "
            //    + "JOIN Crops AS crop "
            //    + "ON crop.CropId = cv.CropId "
            //    + "GROUP BY crop.Name "
            //    + "ORDER BY crop.Name";
            //IEnumerable<CropGroup> data = db.Database.SqlQuery<CropGroup>(query);

            return View(data.ToList());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using Mvc5TestProj.Models;
using Mvc5TestProj.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mvc5TestProj.Controllers
{
    public class ExternalPartnersController : Controller
    {
        private Mvc5TestEntities db = new Mvc5TestEntities();

        // GET: ExternalPartners
        public ActionResult Index(int? id, string sortOrder)
        {
            var ExtPartCrRegVM = new ExternalPartnerIndexViewModel();

            // Eager loading with Include
            ExtPartCrRegVM.ExternalPartners = db.ExternalPartners
                .Include(e => e.Province)
                .Include(i => i.ExternalPartnerCropRegions.Select(epcr => epcr.CropRegion));

            ViewBag.CompanySortParm = String.IsNullOrEmpty(sortOrder) ? "company_desc" : "";
            ViewBag.CitySortParm = sortOrder == "city" ? "city_desc" : "city";

            switch (sortOrder)
            {
                case "company_desc":
                    ExtPartCrRegVM.ExternalPartners = ExtPartCrRegVM.ExternalPartners.OrderByDescending(s => s.CompanyName);
                    break;
                case "city":
                    ExtPartCrRegVM.ExternalPartners = ExtPartCrRegVM.ExternalPartners.OrderBy(s => s.City);
                    break;
                case "city_desc":
                    ExtPartCrRegVM.ExternalPartners = ExtPartCrRegVM.ExternalPartners.OrderByDescending(s => s.City);
                    break;
                default:
                    ExtPartCrRegVM.ExternalPartners = ExtPartCrRegVM.ExternalPartners.OrderBy(s => s.CompanyName);
                    break;
            }

            if (id != null)
            {
                ViewBag.ExternalPartnerId = id.Value;
                // Lazy loading
                //ExtPartCrRegVM.CropRegions = ExtPartCrRegVM.ExternalPartners.Where(
                //    i => i.ExternalPartnerId == id.Value).Single().ExternalPartnerCropRegions.Select(i => i.CropRegion);

                // Explicit loading
                ExternalPartner selectedExternalPartner = ExtPartCrRegVM.ExternalPartners.Where(
                    x => x.ExternalPartnerId == id.Value).Single();
                db.Entry(selectedExternalPartner).Collection(x => x.ExternalPartnerCropRegions).Load();
                foreach (ExternalPartnerCropRegion epcr in selectedExternalPartner.ExternalPartnerCropRegions)
                {
                    db.Entry(epcr).Reference(x => x.CropRegion).Load();
                }

                ExtPartCrRegVM.CropRegions = selectedExternalPartner.ExternalPartnerCropRegions.Select(i => i.CropRegion);
            }

            return View(ExtPartCrRegVM);
        }

        // GET: ExternalPartners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExternalPartner externalPartner = db.ExternalPartners.Find(id);
            if (externalPartner == null)
            {
                return HttpNotFound();
            }
            return View(externalPartner);
        }

        // GET: ExternalPartners/Create
        public ActionResult Create()
        {
            var externalPartner = new ExternalPartner();
            externalPartner.ExternalPartnerCropRegions = new List<ExternalPartnerCropRegion>();
            PopulateAssignedCropRegionViewModel(externalPartner);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "ShortName");
            return View();
        }

        // POST: ExternalPartners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ExternalPartnerId,CompanyName,PhoneNumber,AddressLine1,AddressLine2,City,ProvinceId,PostalCode,Email,SapAccountNo,Website,SCCAccredited,RDVendor,EAVendor,MasterAgreementSent,MasterAgreementSigned,VendorTermsAndConditionsAccepted,Active,Comments")]
            ExternalPartner externalPartner, string[] selectedExternalPartnerCropRegions)
        {
            if (selectedExternalPartnerCropRegions != null)
            {
                externalPartner.ExternalPartnerCropRegions = new List<ExternalPartnerCropRegion>();
                foreach (var extPartCropReg in selectedExternalPartnerCropRegions)
                {
                    var externalPartnerCropRegionsToAdd = db.ExternalPartnerCropRegions.Find(int.Parse(extPartCropReg));
                    externalPartner.ExternalPartnerCropRegions.Add(externalPartnerCropRegionsToAdd);
                }
            }

            if (ModelState.IsValid)
            {
                db.ExternalPartners.Add(externalPartner);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateAssignedCropRegionViewModel(externalPartner);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "ShortName", externalPartner.ProvinceId);
            return View(externalPartner);
        }

        // GET: ExternalPartners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // no eager loading with Find, so replaced with Where, Single.
            // ExternalPartner externalPartner = db.ExternalPartners.Find(id);

            ExternalPartner externalPartner = db.ExternalPartners
                .Include(i => i.Province)
                .Include(i => i.ExternalPartnerCropRegions)
                .Where(i => i.ExternalPartnerId == id)
                .Single();

            PopulateAssignedCropRegionViewModel(externalPartner);

            if (externalPartner == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "ShortName", externalPartner.ProvinceId);
            return View(externalPartner);
        }

        // POST: ExternalPartners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] selectedCropRegions)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ExternalPartner externalPartnerToUpdate = db.ExternalPartners
                     .Include(i => i.Province)
                     .Include(i => i.ExternalPartnerCropRegions)
                     .Where(i => i.ExternalPartnerId == id)
                     .Single();

            if (TryUpdateModel(externalPartnerToUpdate, "",
               new string[] { "ExternalPartnerId","CompanyName","PhoneNumber","AddressLine1","AddressLine2","City","ProvinceId","PostalCode","Email","SapAccountNo","Website","SCCAccredited","RDVendor","EAVendor","MasterAgreementSent","MasterAgreementSigned","VendorTermsAndConditionsAccepted","Active","Comments" }))
            {
                try
                {
                    UpdateExternalPartnerCropRegions(selectedCropRegions, externalPartnerToUpdate);
                    db.Entry(externalPartnerToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DataException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                }
            }

            PopulateAssignedCropRegionViewModel(externalPartnerToUpdate);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "ShortName", externalPartnerToUpdate.ProvinceId);

            return View(externalPartnerToUpdate);
        }

        // GET: ExternalPartners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExternalPartner externalPartner = db.ExternalPartners.Find(id);
            if (externalPartner == null)
            {
                return HttpNotFound();
            }
            return View(externalPartner);
        }

        // POST: ExternalPartners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                // ExternalPartner externalPartner = db.ExternalPartners.Find(id);
                ExternalPartner externalPartner = db.ExternalPartners
                         .Include(i => i.Province)
                         .Include(i => i.ExternalPartnerCropRegions)
                         .Where(i => i.ExternalPartnerId == id)
                         .Single();

                IEnumerable<ExternalPartnerCropRegion> externalPartnerCropRegionsToDel = db.ExternalPartnerCropRegions
                    .Where(i => i.ExternalPartnerId == id);

                foreach (ExternalPartnerCropRegion epcr in externalPartnerCropRegionsToDel)
                {
                    ExternalPartnerCropRegion epcrToDel = db.ExternalPartnerCropRegions.Where(i => i.ExternalPartnerCropRegionId == epcr.ExternalPartnerCropRegionId).Single();
                    db.ExternalPartnerCropRegions.Remove(epcrToDel);
                }

                db.ExternalPartners.Remove(externalPartner);

                db.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                SqlException s = e.InnerException.InnerException as SqlException;
                if (s != null)
                {
                    ModelState.AddModelError("", e.Message);
                    return View("Delete");
                }
                else
                {
                     ModelState.AddModelError(string.Empty,
                                "An error occured - please contact your system administrator.");
                     return View("Delete");
               }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                return View("Delete");
            }

            return RedirectToAction("Index");
        }

        private void PopulateAssignedCropRegionViewModel(ExternalPartner externalPartner)
        {
            var allCropRegions = db.CropRegions;
            var externalPartnerCrRegions = new HashSet<int>(externalPartner.ExternalPartnerCropRegions.Select(cr => cr.CropRegion.CropRegionId));
            var assignedCropRegionVM = new List<AssignedCropRegionViewModel>();
            foreach (var cropRegion in allCropRegions)
            {
                assignedCropRegionVM.Add(new AssignedCropRegionViewModel
                {
                    CropRegionId = cropRegion.CropRegionId,
                    Name = cropRegion.Name,
                    Assigned = externalPartnerCrRegions.Contains(cropRegion.CropRegionId)
                });
            }
            ViewBag.CropRegions = assignedCropRegionVM;
        }

        private void UpdateExternalPartnerCropRegions(string[] selectedCropRegions, ExternalPartner externalPartnerToUpdate)
        {
            if (selectedCropRegions == null)
            {
                externalPartnerToUpdate.ExternalPartnerCropRegions = new List<ExternalPartnerCropRegion>();
                return;
            }

            var selectedCropRegionsSet = new HashSet<string>(selectedCropRegions);
            var externalPartnerCropRegions = new HashSet<int>
                (externalPartnerToUpdate.ExternalPartnerCropRegions.Select(c => c.CropRegionId));
            foreach (var cropRegion in db.CropRegions)
            {
                if (selectedCropRegionsSet.Contains(cropRegion.CropRegionId.ToString()))
                {
                    if (!externalPartnerCropRegions.Contains(cropRegion.CropRegionId))
                    {
                        ExternalPartnerCropRegion epcr = new ExternalPartnerCropRegion()
                        {
                            ExternalPartnerId = externalPartnerToUpdate.ExternalPartnerId,
                            CropRegionId = cropRegion.CropRegionId
                        };
                        externalPartnerToUpdate.ExternalPartnerCropRegions.Add(epcr);
                    }
                }
                else
                {
                    if (externalPartnerCropRegions.Contains(cropRegion.CropRegionId))
                    {
                        ExternalPartnerCropRegion epcrToDelete = db.ExternalPartnerCropRegions.Where(i => i.CropRegionId == cropRegion.CropRegionId && i.ExternalPartnerId == externalPartnerToUpdate.ExternalPartnerId).Single();
                        db.ExternalPartnerCropRegions.Remove(epcrToDelete);
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿namespace Mvc5TestProj.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Mvc5TestProj.Models;
    using Kendo.Mvc.UI;
    using Kendo.Mvc.Extensions;

    public class GridToTreeDraggableController : Controller
    {
        // GET: GridToTreeDraggableTest
        public ActionResult Index()
        {
            List<Fruit> fruits = GetFruits();
            ViewBag.Fruits = fruits;

            return View();
        }

        public ActionResult GridToTreeDraggableTS()
        {
            List<Fruit> fruits = GetFruits();
            ViewBag.Fruits = fruits;
 
            return View();
        }

        public ActionResult Fruits_Read([DataSourceRequest]DataSourceRequest request)
        {
            List<Fruit> fruits = GetFruits();

            //IQueryable<Fruit> allfruits = fruits.AsQueryable();
            DataSourceResult result = fruits.ToDataSourceResult(request);
            return Json(result);
        }

        private List<Fruit> GetFruits()
        {
           List<Fruit> fruits = new List<Fruit>();

            Fruit fruit = new Fruit()
            {
                fruitid = 1,
                name = "Apples",
                responsibleUserid = 1
            };
            fruits.Add(fruit);

            fruit = new Fruit()
            {
                fruitid = 2,
                name = "Oranges",
                responsibleUserid = 2
            };
            fruits.Add(fruit);

            fruit = new Fruit()
            {
                fruitid = 3,
                name = "Pineapples",
                responsibleUserid = 0
            };
            fruits.Add(fruit);

            fruit = new Fruit()
            {
                fruitid = 4,
                name = "Apricots",
                responsibleUserid = 0
            };
            fruits.Add(fruit);

            return fruits;
        }

        public JsonResult Reps_Read()
        {
            List<Rep> reps = new List<Rep>();

            Rep rep = new Rep()
            {
                userid = 1,
                hasChildren = true,
                reportsTo = null,
                name = "John Smith",
                subitems = new List<Rep>()
            };
 
            rep.subitems.Add( new Rep()
            {
                userid = 5,
                hasChildren = false,
                reportsTo = 1,
                name = "Neil Harvey",
                subitems = new List<Rep>()
            });

            rep.subitems.Add( new Rep()
            {
                userid = 2,
                hasChildren = false,
                reportsTo = 1,
                name = "Cindy Smith",
                subitems = new List<Rep>()
            });

            rep.subitems.Add( new Rep()
            {
                userid = 4,
                hasChildren = false,
                reportsTo = 1,
                name = "Norman Cunningham",
                subitems = new List<Rep>()
            });
            reps.Add(rep);

            rep = new Rep()
            {
                userid = 3,
                reportsTo = null,
                hasChildren = true,
                name = "Frank Worrell",
                subitems = new List<Rep>()
            };
            reps.Add(rep);

            rep = new Rep()
            {
                userid = 6,
                reportsTo = null,
                hasChildren = true,
                name = "Gary Sobers",
                subitems = new List<Rep>()
            };

            rep.subitems.Add( new Rep()
            {
                userid = 7,
                reportsTo = 6,
                hasChildren = false,
                name = "Lance Gibbs",
                subitems = new List<Rep>()
            });
            reps.Add(rep);

            // LINQ projection into anonymous object
            var theReps = reps.Select(e => new
            {
                userid = e.userid,
                name = e.name,
                reportsTo = e.reportsTo,
                hasChildren = e.reportsTo == null ? true : e.subitems.Any(),
                subitems = e.subitems.Select(t => new
                {
                    userid = t.userid,
                    name = t.name,
                    reportsTo = t.reportsTo,
                    hasChildren = false
                })
            }).ToList();


            return Json(theReps, JsonRequestBehavior.AllowGet);
        }

    }
}
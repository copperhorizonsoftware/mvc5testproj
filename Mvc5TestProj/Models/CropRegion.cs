//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mvc5TestProj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class CropRegion
    {
        public CropRegion()
        {
            this.ExternalPartnerCropRegions = new HashSet<ExternalPartnerCropRegion>();
        }
    
        [Required]
        public int CropRegionId { get; set; }
    
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    
        public virtual ICollection<ExternalPartnerCropRegion> ExternalPartnerCropRegions { get; set; }
    }
}

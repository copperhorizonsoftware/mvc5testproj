//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mvc5TestProj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class ExternalPartner
    {
        public ExternalPartner()
        {
            this.ExternalPartnerCropRegions = new HashSet<ExternalPartnerCropRegion>();
        }
    
        [Required]
        public int ExternalPartnerId { get; set; }
    
        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }
    
        [Required]
        [StringLength(20)]
        public string PhoneNumber { get; set; }
    
        [Required]
        [StringLength(50)]
        public string AddressLine1 { get; set; }
    
        [StringLength(50)]
        public string AddressLine2 { get; set; }
    
        [Required]
        [StringLength(50)]
        public string City { get; set; }
    
        [Required]
        public int ProvinceId { get; set; }
    
        [Required]
        [StringLength(10)]
        public string PostalCode { get; set; }
    
        [StringLength(80)]
        public string Email { get; set; }
    
        [StringLength(20)]
        public string SapAccountNo { get; set; }
    
        [StringLength(255)]
        public string Website { get; set; }
    
        [Required]
        public bool SCCAccredited { get; set; }
    
        [Required]
        public bool RDVendor { get; set; }
    
        [Required]
        public bool EAVendor { get; set; }
    
        [Required]
        public bool MasterAgreementSent { get; set; }
    
        [Required]
        public bool MasterAgreementSigned { get; set; }
    
        [Required]
        public bool VendorTermsAndConditionsAccepted { get; set; }
    
        [Required]
        public bool Active { get; set; }
    
        [StringLength(1000)]
        public string Comments { get; set; }
    
        public virtual ICollection<ExternalPartnerCropRegion> ExternalPartnerCropRegions { get; set; }
        public virtual Province Province { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mvc5TestProj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class ExternalPartnerCropRegion
    {
        [Required]
        public int ExternalPartnerCropRegionId { get; set; }
    
        [Required]
        public int ExternalPartnerId { get; set; }
    
        [Required]
        public int CropRegionId { get; set; }
    
        public virtual CropRegion CropRegion { get; set; }
        public virtual ExternalPartner ExternalPartner { get; set; }
    }
}

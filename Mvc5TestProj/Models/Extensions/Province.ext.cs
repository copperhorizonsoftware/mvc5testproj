﻿namespace Mvc5TestProj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// Province class.
    /// </summary>
    [MetadataType(typeof(ProvinceMetadata))]
    public partial class Province
    {
        /// <summary>
        /// Province class metadata
        /// </summary>
        internal sealed class ProvinceMetadata
        {
            /// <summary>
            /// Gets or sets the name of the province code
            /// </summary>
            [StringLength(2, MinimumLength = 2)]
            [Display(Name = "Short Name")]
            public string ShortName { get; set; }
            
            /// <summary>
            /// Gets or sets the name of the province
            /// </summary>
            [StringLength(50, MinimumLength = 2)]
            [Display(Name = "Long Name")]
            public string LongName { get; set; }
        }
    }
}

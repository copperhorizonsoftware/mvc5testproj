﻿namespace Mvc5TestProj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Crop variety class extension for annotations
    /// </summary>
    [MetadataType(typeof(CropVarietyMetadata))]
    public partial class CropVariety
    {
        /// <summary>
        /// Crop variety metadata class
        /// </summary>
        internal sealed class CropVarietyMetadata
        {
            /// <summary>
            /// Gets or sets the crop variety id
            /// </summary>
            [ScaffoldColumn(false)]
            public int CropVarietyId { get; set; }

            /// <summary>
            /// Gets or sets the crop id
            /// </summary>
            [Required]
            [Range(1, int.MaxValue, ErrorMessage = "Crop is required.")]
            [Display(Name = "Crop")]    
            public int CropId { get; set; }

            /// <summary>
            /// Gets or sets the crop variety name
            /// </summary>
            [Required(ErrorMessage = ("Crop variety name is required."))]
            [StringLength(50)]
            [Display(Name = "Variety")]  
            public string Name { get; set; }
        }
    }
}
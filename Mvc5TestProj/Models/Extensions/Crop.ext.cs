namespace Mvc5TestProj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// Crop class.
    /// </summary>
    [MetadataType(typeof(CropMetadata))]
    public partial class Crop
    {
        /// <summary>
        /// Crop class metadata
        /// </summary>
        internal sealed class CropMetadata
        {
            /// <summary>
            /// Gets or sets the name of the crop
            /// </summary>
            [Display(Name = "Crop Name")]
            public string Name { get; set; }

            [Display(Name = "Crop Varieties")]
            public ICollection<CropVariety> CropVarieties { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5TestProj.Models
{
    public class Fruit
    {
        public int fruitid { get; set; }
        public int responsibleUserid { get; set; }
        public string name { get; set; }

        public Fruit()
        {
        }
    }
}
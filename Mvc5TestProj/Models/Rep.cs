﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc5TestProj.Models
{
    public class Rep
    {
        public int userid { get; set; }
        public int? reportsTo { get; set; }
        public bool hasChildren { get; set; }
        public string name { get; set; }
        public List<Rep> subitems { get; set; }

        public Rep()
        {
        }
    }
}
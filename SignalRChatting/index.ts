﻿///<reference path='../SignalRChatting/Scripts/typings/jquery/jquery.d.ts' />
///<reference path='../SignalRChatting/Scripts/typings/signalr/signalr.d.ts' />

interface signalR extends JQueryStatic {
    connection;
}

// update the page and send messages.
$(function () {
    alert("here");
    // Declare a proxy to reference the hub - references the C# ChatHub class in JavaScript as chatHub.
    var chat = (<signalR>$).connection.chatHub;

    // Get the user name and store it to prepend to messages.
    $('#displayname').val(prompt('Enter your name:', ''));
    // Set initial focus to message input box.
    $('#message').focus();

    // Start the connection. Note: This approach ensures that the connection is established before the event handler executes. 
    // The following code shows how to open a connection with the hub. The code starts the connection and then passes it a function to handle the click event on the Send button in the HTML page.
    (<signalR>$).connection.hub.start().done(function () {
        $('#sendmessage').click(function () {
            // Call the Send method on the hub.
            chat.server.send($('#displayname').val(), $('#message').val());
            $('#message').css("background-color", "lightblue");
            // Clear text box and reset focus for next comment.
            $('#message').val('').focus();
        });
    });

    // Create a callback function in the script that the hub can call to broadcast messages. The hub class on the server calls this function to push content updates to each client.
    chat.client.broadcastMessage = function (name, message) {
        // Html encode display name and message. A simple way to prevent script injection.
        var encodedName = $('<div />').text(name).html();
        var encodedMsg = $('<div />').text(message).html();
        // Add the message to the page.
        $('#discussion').append('<li><strong>' + encodedName
            + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
    };
});  